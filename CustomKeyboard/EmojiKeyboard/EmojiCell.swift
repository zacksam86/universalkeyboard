//
//  EmojiCell.swift
//  CustomKeyboard
//
//  Created by  on 2017-05-11.
//  Copyright © 2017 mobilelive. All rights reserved.
//

import UIKit

class EmojiCell: UICollectionViewCell {
    
    @IBOutlet weak var imgEmoji: UIImageView!
}
