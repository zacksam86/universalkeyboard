//
//  KeyboardViewController.swift
//  EmojiKeyboard
//
//  Created by  on 2017-05-11.
//  Copyright © 2017 mobilelive. All rights reserved.
//

import UIKit


class KeyboardViewController: UIInputViewController {

    @IBOutlet var nextKeyboardButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var banner: UIView!
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    var nextKeyboardButtonLeftSideConstraint: NSLayoutConstraint!
    var heightConstraint: NSLayoutConstraint?
    
    let iphone_4_5_keyboard_height :CGFloat = 253
    let iphone_6_keyboard_height :CGFloat = 258
    let iphone_6_plus_keyboard_height :CGFloat = 271

    let emjojiArray = NSMutableArray()
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        // Add custom view sizing constraints here
        if (view.frame.size.width == 0 || view.frame.size.height == 0) {
            return
        }
        setUpHeightConstraint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: collectionView.frame.width * 0.005, left: collectionView.frame.width * 0.005, bottom: 0, right: 0)
        
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        collectionView!.collectionViewLayout = layout
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "KeyboardView", bundle: nil)
        let objects = nib.instantiate(withOwner: self, options: nil)
        self.view = objects[0] as! UIView;
        
        let nibName = UINib(nibName: "EmojiCell", bundle:nil)
        collectionView.register(nibName, forCellWithReuseIdentifier: "emojicell")
        collectionView.translatesAutoresizingMaskIntoConstraints = false;
        bannerHeight.constant = 0;
        
        //Add emoji file names to array
        for index in 1...24{
            emjojiArray.add("\(index).png")
        }
        
        // Perform custom UI setup here
        self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = false;
        self.nextKeyboardButton.addTarget(self, action: #selector(handleInputModeList(from:with:)), for: .allTouchEvents)
    }
    
    // MARK: Set up height constraint
    func setUpHeightConstraint()
    {
        var customHeight : CGFloat = iphone_6_keyboard_height
        
        switch UIDevice.current.type {
        case .iPhone4:
            customHeight = iphone_4_5_keyboard_height
        case .iPhone5:
            customHeight = iphone_4_5_keyboard_height
        case .iPhone5S:
            customHeight = iphone_4_5_keyboard_height
        case .iPhone6:
            customHeight = iphone_6_keyboard_height
        case .iPhone6plus:
            customHeight = iphone_6_plus_keyboard_height
        case .iPhone6S:
            customHeight = iphone_6_keyboard_height
        case .iPhone6Splus:
            customHeight = iphone_6_plus_keyboard_height
        default:
            print("I am not equipped to handle this device")
        }
        
        if heightConstraint == nil {
            heightConstraint = NSLayoutConstraint(item: view,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: .notAnAttribute,
                                                  multiplier: 1,
                                                  constant: customHeight)
            heightConstraint?.priority = UILayoutPriority(UILayoutPriorityRequired)
            
            heightConstraint?.priority = 999
            heightConstraint?.isActive = true
            
            view.addConstraint(heightConstraint!)
            
            collectionView.updateConstraintsIfNeeded()
        }
        else {
            heightConstraint?.constant = customHeight
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }
    
    override func textWillChange(_ textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }
    
    override func textDidChange(_ textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
        
        var textColor: UIColor
        let proxy = self.textDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.dark {
            textColor = UIColor.white
        } else {
            textColor = UIColor.black
        }
        self.nextKeyboardButton.setTitleColor(textColor, for: [])
    }
    
    @IBAction func backSpacePressed(button: UIButton) {
        (textDocumentProxy as UIKeyInput).deleteBackward()
    }
    
    func showBanner()  {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.bannerHeight.constant = 35;
        }) { (finished) in
            self.perform(#selector(self.hideBanner), with: nil, afterDelay: 1.0)
        }
    }
    
    func hideBanner()  {
        
        UIView.animate(withDuration: 0.25, animations: {
            
            self.bannerHeight.constant = 0;
        })
    }
}

extension KeyboardViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        var height = (collectionView.frame.height - collectionView.frame.width * 0.015) * 0.1575
        
        if height < 60{
            height = (collectionView.frame.height - collectionView.frame.width * 0.015) * 0.65
        }
        
        return CGSize(width: collectionView.frame.width * 0.98 / 5 , height: height)
    }
}

extension KeyboardViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
     
        let image = UIImage(named: ("\(indexPath.row+1)_large.png"))
        UIPasteboard.general.image = image
        
        showBanner()
    }
}

extension KeyboardViewController: UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return emjojiArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:EmojiCell = collectionView.dequeueReusableCell(withReuseIdentifier: "emojicell", for: indexPath) as! EmojiCell
        cell.imgEmoji.image = UIImage(named: emjojiArray[indexPath.row] as! String)
        return cell
    }
    
}
