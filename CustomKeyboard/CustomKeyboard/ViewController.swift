//
//  ViewController.swift
//  CustomKeyboard
//
//  Created by  on 2017-05-11.
//  Copyright © 2017 mobilelive. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    let emjojiArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let nibName = UINib(nibName: "EmojiCell", bundle:nil)
        collectionView.register(nibName, forCellWithReuseIdentifier: "emojicell")
        //collectionView.delegate = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //let width = UIScreen.main.bounds.width
        
        layout.sectionInset = UIEdgeInsets(top: collectionView.frame.width * 0.01, left: collectionView.frame.width * 0.01, bottom: 0, right: 0)
        
        layout.itemSize = CGSize(width: collectionView.frame.width * 0.98 / 3 , height: collectionView.frame.height * 0.97)
        
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        collectionView!.collectionViewLayout = layout
        collectionView.dataSource = self
        
        //Add emoji file names to array
        for index in 1...24{
            emjojiArray.add("\(index).png")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnPrevClicked(_ sender: Any) {
        
        var contentOffSetX : CGFloat = self.collectionView.contentOffset.x - self.collectionView.frame.width
        
        if contentOffSetX < 0{
            contentOffSetX = 0
        }
        
        let offset = CGPoint(x: contentOffSetX, y: self.collectionView.contentOffset.y)
        self.collectionView.setContentOffset(offset, animated: true)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        var contentOffSetX : CGFloat = self.collectionView.contentOffset.x + self.collectionView.frame.width
        if contentOffSetX > self.collectionView.contentSize.width {
            contentOffSetX = self.collectionView.contentSize.width - self.collectionView.frame.width
        }
        
        let offset = CGPoint(x: contentOffSetX, y: self.collectionView.contentOffset.y)
        
        self.collectionView.setContentOffset(offset, animated: true)
    }   
}

extension ViewController: UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return emjojiArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:EmojiCell = collectionView.dequeueReusableCell(withReuseIdentifier: "emojicell", for: indexPath) as! EmojiCell
        cell.imgEmoji.image = UIImage(named: emjojiArray[indexPath.row] as! String)
        return cell
    }
    
}

