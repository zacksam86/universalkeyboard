//
//  PickEmojiController.swift
//  CustomKeyboard
//
//  Created by  on 2017-05-15.
//  Copyright © 2017 mobilelive. All rights reserved.
//

import UIKit

class PickEmojiController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    let emjojiArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        let nibName = UINib(nibName: "EmojiCell", bundle:nil)
        collectionView.register(nibName, forCellWithReuseIdentifier: "emojicell")
        collectionView.delegate = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //let width = UIScreen.main.bounds.width
        
        layout.sectionInset = UIEdgeInsets(top: collectionView.frame.width * 0.01, left: collectionView.frame.width * 0.01, bottom: 0, right: 0)
        
        layout.itemSize = CGSize(width: collectionView.frame.width * 0.98 / 4 , height: collectionView.frame.width * 0.98 / 4)
        
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .vertical
        collectionView!.collectionViewLayout = layout
        collectionView.dataSource = self
        
        //Add emoji file names to array
        for index in 1...24{
            emjojiArray.add("\(index).png")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func shareImage(image : UIImage) {
        
        let shareItems:Array = [image]
        let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension PickEmojiController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        //let image = UIImage(named: emjojiArray[indexPath.row] as! String)
        let image = UIImage(named: ("\(indexPath.row+1)_large.png"))
        shareImage(image: image!)
    }
}

extension PickEmojiController: UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return emjojiArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:EmojiCell = collectionView.dequeueReusableCell(withReuseIdentifier: "emojicell", for: indexPath) as! EmojiCell
        cell.imgEmoji.image = UIImage(named: emjojiArray[indexPath.row] as! String)
        return cell
    }
    
}
